;;; csv-nav-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (csv-nav-mode) "csv-nav" "csv-nav.el" (21286 30165
;;;;;;  652061 371000))
;;; Generated autoloads from csv-nav.el

(autoload 'csv-nav-mode "csv-nav" "\
Major mode for viewing and editing CSV files.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("csv-nav-pkg.el") (21286 30165 670497
;;;;;;  784000))

;;;***

(provide 'csv-nav-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; csv-nav-autoloads.el ends here
